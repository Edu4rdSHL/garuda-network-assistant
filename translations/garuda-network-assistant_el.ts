<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Garuda Network Assistant</source>
        <translation>Garuda Network Assistant</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Status</source>
        <translation>Κατάσταση:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="76"/>
        <source>IP address</source>
        <translation>Διεύθυνση IP</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>Hardware detected</source>
        <translation>ανιχνεύθηκε το υλικό</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="389"/>
        <location filename="../mainwindow.ui" line="502"/>
        <source>Re-scan</source>
        <translation>επαν-σάρωσης</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Active interface</source>
        <translation>Ενεργή διασύνδεση</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>WiFi status</source>
        <translation>Κατάσταση WiFi</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Ξεμπλοκάρει όλα τα μαλακά/σκληρά μπλοκάρεσμενα Ασύρματες Συσκευές</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Unblock WiFi Devices</source>
        <translation>Ξεμπλοκάρει Συσκευές WIFI</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="288"/>
        <source>Linux drivers</source>
        <translation>οδηγοί του Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Associated Linux drivers</source>
        <translation>Associated Linux drivers</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="309"/>
        <source>Load Driver</source>
        <translation>Φορτώση του Driver</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="372"/>
        <source>Unload Driver</source>
        <translation>Ξεφορτώση του Driver</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="409"/>
        <source>Blacklist Driver</source>
        <translation>Προγραφή του Driver
 </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>Windows drivers</source>
        <translation>οδηγοί του Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Available Windows drivers</source>
        <translation>Διαθέσιμα drivers του Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="468"/>
        <source>Remove Driver</source>
        <translation>Αφαίρεση του Driver</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="479"/>
        <source>Add Driver</source>
        <translation>Πρόσθεση του Driver</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="520"/>
        <source>About NDISwrapper</source>
        <translation>Σχετικά με το NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="532"/>
        <source>Install NDISwrapper</source>
        <translation>Εγκατάσταση του NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="543"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>Για να χρησιμοποιήσετε τα οδηγοί των Windows θα πρέπει πρώτα να εγκαταστήσετε το πακέτο NDISwrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="559"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Απεγκατάσταση του NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="591"/>
        <source>Net diagnostics</source>
        <translation>Διάγνωση του διαδίκτυο</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="603"/>
        <source>Ping</source>
        <translation>Ping</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="609"/>
        <location filename="../mainwindow.ui" line="725"/>
        <source>Target URL:</source>
        <translation>Στόχος URL:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Packets</source>
        <translation>Πακέτα</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="661"/>
        <location filename="../mainwindow.ui" line="780"/>
        <source>Start</source>
        <translation>Ξεκινήστε</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="797"/>
        <source>Clear</source>
        <translation>Εκκαθάριση</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="692"/>
        <location filename="../mainwindow.ui" line="811"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="719"/>
        <source>Traceroute</source>
        <translation>Traceroute</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="735"/>
        <source>Hops</source>
        <translation>Hops</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="866"/>
        <source>About...</source>
        <translation>Περί</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Help</source>
        <translation>Βοήθεια </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="957"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="964"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>Διεύθυνση IP από το router:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Εξωτερική διεύθυνση IP:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <location filename="../mainwindow.cpp" line="239"/>
        <source>&amp;Copy</source>
        <translation>&amp;Αντιγραφή</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="213"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="214"/>
        <location filename="../mainwindow.cpp" line="228"/>
        <location filename="../mainwindow.cpp" line="242"/>
        <source>Copy &amp;All</source>
        <translation>Αντιγραφή &amp;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="216"/>
        <location filename="../mainwindow.cpp" line="230"/>
        <location filename="../mainwindow.cpp" line="244"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="281"/>
        <location filename="../mainwindow.cpp" line="303"/>
        <source>Traceroute not installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Traceroute δεν έχει εγκατασταθεί, Θέλετε να το εγκαταστήσετε τώρα ;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="290"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Traceroute δεν μπορεί να εγκατασταθεί. Μπορεί αυτό να σημαίνει ότι χρησιμοποιείτε το LiveCD ή δεν μπορείτε να φτάσετε στο αποθετήριο λογισμικού,</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Traceroute δεν έχει εγκατασταθεί και δεν υπάρχει σύνδεση στο Internet, ώστε να μην μπορεί να εγκατασταθεί</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>No destination host</source>
        <translation>Δεν υπαρχει host προορισμού</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please fill in the destination host field</source>
        <translation>Παρακαλούμε συμπληρώστε το πεδίο host προορισμού</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="493"/>
        <source>Loaded Drivers</source>
        <translation>Φορτωμένο Driver&#xa0;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="503"/>
        <source>Unloaded Drivers</source>
        <translation>Εκφορτωμένο Driver </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="519"/>
        <source>Blacklisted Drivers</source>
        <translation>Προγραφόμενα Drivers</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>Blacklisted Broadcom Drivers</source>
        <translation>Προγραφόμενα Drivers του Broadcom</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1012"/>
        <source>enabled</source>
        <translation>ενεργοποιημένο</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1015"/>
        <source>disabled</source>
        <translation>απενεργοποιημένο</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1018"/>
        <source>WiFi hardware switch is off</source>
        <translation>Ο διακόπτης υλικού WiFi είναι απενεργοποιημένος</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Εντοπίστε το οδήγo των Windows που θέλετε να προσθέσετε</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Windows Installer αρχείο πληροφοριών (*.inf)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>*.sys file not found</source>
        <translation>Το αρχείο *.sys  δε βρέθηκε</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>Τα αρχεία *.sys πρέπει να είναι στο ιδιο μερος το αρχειο * .inf. 1% δεν μπορεί να βρεθεί</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>sys file reference not found</source>
        <translation>δε βρέθηκε το αρχείο sys</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>Το αρχείο sys για το οδήγo δεν μπορεί να προσδιοριστεί μετά ανάλυση του αρχείου .inf</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>Το οδηγό Ndiswrapper έχει αφαιρεθεί.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1135"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1136"/>
        <source>Version: </source>
        <translation>Έκδοση:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1137"/>
        <source>Program for troubleshooting and configuring network for Garuda Linux</source>
        <translation>Eφαρμογή για την αντιμετώπιση προβλημάτων δικτύου και τη διαμόρφωση.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1139"/>
        <source>Copyright (c) MEPIS LLC and Garuda Linux</source>
        <translation>Copyright (c) MEPIS LLC and Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1140"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Άδεια</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Καταγραφή αλλαγών</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Ακύρωση</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Κλείσιμο</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Πρέπει να τρέξετε αυτή την εφαρμογή ως Root.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="565"/>
        <source>Ndiswrapper is not installed</source>
        <translation>Το Ndiswrapper δεν είναι εγκατεστημένο </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>driver installed</source>
        <translation>Το driver εγκαταστάθηκε</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source> and in use by </source>
        <translation>και σε χρήση από</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>. Alternate driver: </source>
        <translation>Εναλλακτικό driver: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Driver removed from blacklist</source>
        <translation>Αφαίρεση του driver από τη μαύρη λίστα</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Driver removed from blacklist.</source>
        <translation>Αφαίρεση του driver από τη μαύρη λίστα</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="698"/>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>Module blacklisted</source>
        <translation>Module blacklisted</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="892"/>
        <source>Installation successful</source>
        <translation>Εγκατάσταση με επιτυχια</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="896"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Εντοπίστηκε σφάλμα, δεν θα μπορούσαν να καταρτίσουν πρόγραμμα οδήγησης ndiswrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="901"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Εντοπίστηκε σφάλμα, δεν θα μπορούσε να εγκαταστήσετε το πακέτο ndiswrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="913"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>Προέκυψε σφάλμα κατά την αφαίρεση του ndiswrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Unblacklist Driver</source>
        <translation>Unblacklist Driver</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="976"/>
        <source>Blacklist Driver</source>
        <translation>Προγραφή του Driver
 </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Δεν θα μπορούσε να ξεκλειδώσει τις συσκευές.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1120"/>
        <source>WiFi devices unlocked.</source>
        <translation>Συσκευές WiFi εχει ξεκλειδωθεί.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1165"/>
        <location filename="../mainwindow.cpp" line="1166"/>
        <source>Driver loaded successfully</source>
        <translation>Το Driver φορτώθηκε επιτυχώς</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <location filename="../mainwindow.cpp" line="1185"/>
        <source>Driver unloaded successfully</source>
        <translation>Το Driver ξεφορτώθηκε επιτυχώς
</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="718"/>
        <source>Could not load </source>
        <translation>Δεν ήταν δυνατή η φόρτωση</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Could not unload </source>
        <translation>Δεν ήταν δυνατή η ξεφόρτωση</translation>
    </message>
</context>
</TS>
