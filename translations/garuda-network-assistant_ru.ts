<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="26"/>
        <source>Garuda Network Assistant</source>
        <translation>Garuda Network Assistant</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="70"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="76"/>
        <source>IP address</source>
        <translation>IP-адрес</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="112"/>
        <source>Hardware detected</source>
        <translation>Оборудование обнаружено</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="143"/>
        <location filename="../mainwindow.ui" line="389"/>
        <location filename="../mainwindow.ui" line="502"/>
        <source>Re-scan</source>
        <translation>Повторить сканирование</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="215"/>
        <source>Active interface</source>
        <translation>Активный интерфейс</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="231"/>
        <source>WiFi status</source>
        <translation>Статус WiFi</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="253"/>
        <source>Unblocks all soft/hard blocked wireless devices</source>
        <translation>Разблокирует все программно/аппаратно заблокированные беспроводные устройства</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="256"/>
        <source>Unblock WiFi Devices</source>
        <translation>Разблокировать WiFi устройства</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="288"/>
        <source>Linux drivers</source>
        <translation>Linux драйверы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="300"/>
        <source>Associated Linux drivers</source>
        <translation>Ассоциированные Linux драйверы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="309"/>
        <source>Load Driver</source>
        <translation>Загрузить драйвер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="372"/>
        <source>Unload Driver</source>
        <translation>Выгрузить драйвер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="409"/>
        <source>Blacklist Driver</source>
        <translation>Драйвер из черного списка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="428"/>
        <source>Windows drivers</source>
        <translation>Windows драйверы</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="434"/>
        <source>Available Windows drivers</source>
        <translation>Доступные драйвера под Windows</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="468"/>
        <source>Remove Driver</source>
        <translation>Удалить драйвер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="479"/>
        <source>Add Driver</source>
        <translation>Добавить драйвер</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="520"/>
        <source>About NDISwrapper</source>
        <translation>О программе NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="532"/>
        <source>Install NDISwrapper</source>
        <translation>Установить NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="543"/>
        <source>In order to use Windows drivers you need first to install NDISwrapper</source>
        <translation>Для использования драйверов под Windows необходимо сначала установить NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="559"/>
        <source>Uninstall NDISwrapper</source>
        <translation>Удалить NDISwrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="591"/>
        <source>Net diagnostics</source>
        <translation>Диагностика сети</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="603"/>
        <source>Ping</source>
        <translation>Пинг</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="609"/>
        <location filename="../mainwindow.ui" line="725"/>
        <source>Target URL:</source>
        <translation>URL назначения:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="619"/>
        <source>Packets</source>
        <translation>Пакеты</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="661"/>
        <location filename="../mainwindow.ui" line="780"/>
        <source>Start</source>
        <translation>Старт</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="678"/>
        <location filename="../mainwindow.ui" line="797"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="692"/>
        <location filename="../mainwindow.ui" line="811"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="719"/>
        <source>Traceroute</source>
        <translation>Трассировка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="735"/>
        <source>Hops</source>
        <translation>Прыжки</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="866"/>
        <source>About...</source>
        <translation>O...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="889"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="957"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="964"/>
        <source>Alt+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="87"/>
        <source>IP address from router:</source>
        <translation>IP-адрес от маршрутизатора:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="88"/>
        <source>External IP address:</source>
        <translation>Внешний IP=адрес:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <location filename="../mainwindow.cpp" line="225"/>
        <location filename="../mainwindow.cpp" line="239"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="213"/>
        <location filename="../mainwindow.cpp" line="227"/>
        <location filename="../mainwindow.cpp" line="241"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="214"/>
        <location filename="../mainwindow.cpp" line="228"/>
        <location filename="../mainwindow.cpp" line="242"/>
        <source>Copy &amp;All</source>
        <translation>Копировать &amp;Все</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="216"/>
        <location filename="../mainwindow.cpp" line="230"/>
        <location filename="../mainwindow.cpp" line="244"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="281"/>
        <location filename="../mainwindow.cpp" line="303"/>
        <source>Traceroute not installed</source>
        <translation>Программа traceroute не установлена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="282"/>
        <source>Traceroute is not installed, do you want to install it now?</source>
        <translation>Программа traceroute не установлена, вы хотите установить ее сейчас?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="290"/>
        <source>Traceroute hasn&apos;t been installed</source>
        <translation>Программа traceroute не была установлена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="291"/>
        <source>Traceroute cannot be installed. This may mean you are using the LiveCD or you are unable to reach the software repository,</source>
        <translation>Программа traceroute не может быть установлена. Возможно, Вы используете LiveCD, или Вам не удалось подключиться к репозиторию.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="304"/>
        <source>Traceroute is not installed and no Internet connection could be detected so it cannot be installed</source>
        <translation>Программа traceroute не установлена, и не обнаружено подключение к Интернету, поэтому она не может быть установлена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="361"/>
        <source>No destination host</source>
        <translation>Нет компьютера назначения</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="362"/>
        <source>Please fill in the destination host field</source>
        <translation>Пожалуйста, заполните поле компьютера назначения</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="493"/>
        <source>Loaded Drivers</source>
        <translation>Загруженные драйвера</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="503"/>
        <source>Unloaded Drivers</source>
        <translation>Выгруженные драйвера</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="519"/>
        <source>Blacklisted Drivers</source>
        <translation>Драйвера в черном списке</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="542"/>
        <source>Blacklisted Broadcom Drivers</source>
        <translation>Драйвера Broadcom из черного списка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1012"/>
        <source>enabled</source>
        <translation>активирован</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1015"/>
        <source>disabled</source>
        <translation>отключен</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1018"/>
        <source>WiFi hardware switch is off</source>
        <translation>WiFi отключен аппаратным переключателем</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Locate the Windows driver you want to add</source>
        <translation>Найдите Windows драйвер, который Вы хотите добавить</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1026"/>
        <source>Windows installation information file (*.inf)</source>
        <translation>Файл установочной информации Windows (*.inf)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>*.sys file not found</source>
        <translation>*.sys файл не найден</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1058"/>
        <source>The *.sys files must be in the same location as the *.inf file. %1 cannot be found</source>
        <translation>Файл *.sys должен быть в том же местоположении, что и *.inf файл. %1 не может быть найден</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>sys file reference not found</source>
        <translation>Ссылка на sys файл не найдена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1069"/>
        <source>The sys file for the given driver cannot be determined after parsing the inf file</source>
        <translation>Файл sys для этого драйвера не может быть определен после анализа inf файла</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <source>Ndiswrapper driver removed.</source>
        <translation>Ndiswrapper драйвер удален.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>%1 Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1135"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1136"/>
        <source>Version: </source>
        <translation>Версия: </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1137"/>
        <source>Program for troubleshooting and configuring network for Garuda Linux</source>
        <translation>Программа для поиска и устранения неисправностей и конфигурации сети.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1139"/>
        <source>Copyright (c) MEPIS LLC and Garuda Linux</source>
        <translation>Авторское право (c) MEPIS LLC и Garuda Linux</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1140"/>
        <source>%1 License</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../about.cpp" line="32"/>
        <source>License</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="33"/>
        <location filename="../about.cpp" line="43"/>
        <source>Changelog</source>
        <translation>Список изменений</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="51"/>
        <source>&amp;Close</source>
        <translation>&amp;Закрыть</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="44"/>
        <source>You must run this program as root.</source>
        <translation>Вы должны запустить программу от имени суперпользователя.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="565"/>
        <source>Ndiswrapper is not installed</source>
        <translation>Ndiswrapper не установлена</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="577"/>
        <source>driver installed</source>
        <translation>драйвер установлен</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="588"/>
        <source> and in use by </source>
        <translation>и используется</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="597"/>
        <source>. Alternate driver: </source>
        <translation>. Альтернативный драйвер:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="689"/>
        <source>Driver removed from blacklist</source>
        <translation>Драйвер удален из черного списка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="690"/>
        <source>Driver removed from blacklist.</source>
        <translation>Драйвер удален из черного списка.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="698"/>
        <location filename="../mainwindow.cpp" line="699"/>
        <source>Module blacklisted</source>
        <translation>Модуль помещен в черный список</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="892"/>
        <source>Installation successful</source>
        <translation>Установка завершилась успешно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="896"/>
        <source>Error detected, could not compile ndiswrapper driver.</source>
        <translation>Обнаружена ошибка, невозможно скомпилировать драйвер NdisWrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="901"/>
        <source>Error detected, could not install ndiswrapper.</source>
        <translation>Обнаружена ошибка, невозможно установить драйвер NdisWrapper.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="913"/>
        <source>Error encountered while removing Ndiswrapper</source>
        <translation>Ошибка при удалении Ndiswrapper</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="971"/>
        <source>Unblacklist Driver</source>
        <translation>Драйвер не из черного списка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="976"/>
        <source>Blacklist Driver</source>
        <translation>Драйвер из черного списка</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Could not unlock devices.
WiFi device(s) might already be unlocked.</source>
        <translation>Не удалось разблокировать устройства.
Wi-Fi устройство(а) может быть уже разблокировано.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1120"/>
        <source>WiFi devices unlocked.</source>
        <translation>WiFi устройства разблокированы.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1165"/>
        <location filename="../mainwindow.cpp" line="1166"/>
        <source>Driver loaded successfully</source>
        <translation>Драйвер загружен успешно</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1184"/>
        <location filename="../mainwindow.cpp" line="1185"/>
        <source>Driver unloaded successfully</source>
        <translation>Драйвер выгружен успешно</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="718"/>
        <source>Could not load </source>
        <translation>Невозможно загрузить</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="754"/>
        <source>Could not unload </source>
        <translation>Невозможно выгрузить</translation>
    </message>
</context>
</TS>
