 #!/bin/bash
interface=$(hwinfo --netcard | grep "Device File" | cut -d " " -f5 | grep e)
$(ethtool -s $interface wol d) &&
notify-send -i "network" 'Wake On Lan' 'turned off via ethtool'

rm -rf /etc/udev/rules.d/81-wol.rules
#echo ACTION==\"add\", SUBSYSTEM==\"net\", NAME==\"e*\", RUN+=\"/usr/bin/ethtool -s \$name\ wol d\" >> /etc/udev/rules.d/81-wol.rules
