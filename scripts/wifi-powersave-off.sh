 #!/bin/bash
interface=$(hwinfo --wlan | grep "Device File" | cut -d " " -f5 )
rm -rf /etc/udev/rules.d/81-wifi-powersave.rules
if [[ $(iw dev $interface get power_save | awk '/Power save/ { print $NF }') == "on" ]]; then
iw dev $interface set power_save off
rm -rf /etc/udev/rules.d/81-wifi-powersave.rules
echo ACTION==\"add\", SUBSYSTEM==\"net\", NAME==\"w*\", RUN+=\"/usr/bin/iw dev \$name\ set power_save off\" >> /etc/udev/rules.d/81-wifi-powersave.rules
notify-send -i "network" 'Wifi power management' 'turned off via iw'

elif [[ $(iwconfig $interface | grep -A1 Power | awk '/Power/ { print $NF }') == "Management:on" ]]; then
iwconfig $interface power off
rm -rf /etc/udev/rules.d/81-wifi-powersave.rules
echo ACTION==\"add\", SUBSYSTEM==\"net\", NAME==\"w*\", RUN+=\"/usr/bin/iwconfig \$name\ power off\" >> /etc/udev/rules.d/81-wifi-powersave.rules
notify-send -i "network" 'Wifi power management' 'turned off via iwconfig'
fi
