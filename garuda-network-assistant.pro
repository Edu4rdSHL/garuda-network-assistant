greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
TARGET = garuda-network-assistant

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# Disable QDebug on Release build
#CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Unused not working
#CONFIG(release):DEFINES += QT_NO_DEBUG_OUTPUT

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

       help.path=$$PREFIX//share/doc/garuda-boot-options/help/
       help.files=help/garuda-network-assistant.html

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "garuda-network-assistant.desktop"

        icons.path = /usr/share/icons/hicolor/scalable/apps/
        icons.files = garuda-network-assistant.png
        
        SCRIPTS_PATH = $$PREFIX/share/garuda/

        scripts.files = scripts
        scripts.path  = $$SCRIPTS_PATH

        INSTALLS += target  desktop help icons scripts
}


TRANSLATIONS += translations/garuda-network-assistant_am.ts \
                translations/garuda-network-assistant_ar.ts \
                translations/garuda-network-assistant_bg.ts \
                translations/garuda-network-assistant_ca.ts \
                translations/garuda-network-assistant_cs.ts \
                translations/garuda-network-assistant_da.ts \
                translations/garuda-network-assistant_de.ts \
                translations/garuda-network-assistant_el.ts \
                translations/garuda-network-assistant_es.ts \
                translations/garuda-network-assistant_et.ts \
                translations/garuda-network-assistant_eu.ts \
                translations/garuda-network-assistant_fa.ts \
                translations/garuda-network-assistant_fi.ts \
                translations/garuda-network-assistant_fr.ts \
                translations/garuda-network-assistant_he_IL.ts \
                translations/garuda-network-assistant_hi.ts \
                translations/garuda-network-assistant_hr.ts \
                translations/garuda-network-assistant_hu.ts \
                translations/garuda-network-assistant_id.ts \
                translations/garuda-network-assistant_is.ts \
                translations/garuda-network-assistant_it.ts \
                translations/garuda-network-assistant_ja.ts \
                translations/garuda-network-assistant_kk.ts \
                translations/garuda-network-assistant_ko.ts \
                translations/garuda-network-assistant_lt.ts \
                translations/garuda-network-assistant_mk.ts \
                translations/garuda-network-assistant_mr.ts \
                translations/garuda-network-assistant_nb.ts \
                translations/garuda-network-assistant_nl.ts \
                translations/garuda-network-assistant_pl.ts \
                translations/garuda-network-assistant_pt.ts \
                translations/garuda-network-assistant_pt_BR.ts \
                translations/garuda-network-assistant_ro.ts \
                translations/garuda-network-assistant_ru.ts \
                translations/garuda-network-assistant_sk.ts \
                translations/garuda-network-assistant_sl.ts \
                translations/garuda-network-assistant_sq.ts \
                translations/garuda-network-assistant_sr.ts \
                translations/garuda-network-assistant_sv.ts \
                translations/garuda-network-assistant_tr.ts \
                translations/garuda-network-assistant_uk.ts \
                translations/garuda-network-assistant_zh_CN.ts \
                translations/garuda-network-assistant_zh_TW.ts

FORMS += \
    mainwindow.ui
HEADERS += \
    mainwindow.h \
    version.h \
    about.h \
    cmd.h
SOURCES += main.cpp \
    mainwindow.cpp \
    about.cpp \
    cmd.cpp
CONFIG += release warn_on thread qt c++11

RESOURCES += \
    images.qrc

